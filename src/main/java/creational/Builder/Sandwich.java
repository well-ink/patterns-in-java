package creational.Builder;

/**
 * Created by ink on 11.10.16.
 */
public class Sandwich {
    private String meat;
    private String bread;
    private String commodities;
    private String sauce;

    public static class Builder {
        private String meat;
        private String bread;
        private String commodities;
        private String sauce;

        public Builder meat(String meat) {
            this.meat = meat;
            return this;
        }

        public Builder bread(String bread) {
            this.bread = bread;
            return this;
        }

        public Builder commodities(String commodities) {
            this.commodities = commodities;
            return this;
        }

        public Builder sauce(String sauce) {
            this.sauce = sauce;
            return this;
        }

        public Builder() {
        }

        public Sandwich build() {
            return new Sandwich(meat, bread, commodities, sauce);
        }
    }

    public Sandwich(String meat, String bread, String commodities, String sauce) {
        this.meat = meat;
        this.bread = bread;
        this.commodities = commodities;
        this.sauce = sauce;
    }

    public String getMeat() {
        return meat;
    }

    public String getBread() {
        return bread;
    }

    public String getCommodities() {
        return commodities;
    }

    public String getSauce() {
        return sauce;
    }
}
