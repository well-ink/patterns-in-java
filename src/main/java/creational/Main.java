package creational;

import creational.AbstractFactory.Food;
import creational.AbstractFactory.FoodFactory;
import creational.AbstractFactory.FoodType;
import creational.Builder.Sandwich;
import creational.Factory.Website;
import creational.Factory.WebsiteFactory;
import creational.Factory.WebsiteType;
import creational.Prototype.Book;
import creational.Prototype.Movie;
import creational.Prototype.Registry;
import creational.Singleton.Singleton;

/**
 * Created by ink on 11.10.16.
 */
public class Main {
    public static void main(String[] args) {
        //Singleton
        Singleton singleton = Singleton.getInstance();  //Синглтон всегда должен быть в единственном экземпляре, пример демонстрирует
                                                        // что две экземпляра по сути являются одним и тем же объектом.
                                                        // Примером синглтона является рантайм и логгер.
        Singleton anotherOne = Singleton.getInstance();

        if (singleton == anotherOne) {
            System.out.println("They are the same!");
            System.out.println(singleton);
            System.out.println(anotherOne);
        }

        System.out.println("=========================");
        //Builder
        // Паттерн строитель используется для ухода от большого количества конструкторов у класса с большим количеством полей.
        // так же, объект предоставляемый строителем являемся неизменяемым. Есть возможность указать
        // только необходимые поля, без нужны создавать специальный конструктор
        Sandwich.Builder builder = new Sandwich.Builder();
        builder.bread("white").meat("cow").commodities("wasabi");
        Sandwich sandwich = builder.build();
        // Пример из Java API - StringBuilder
        StringBuilder stringBuilder = new StringBuilder(sandwich.getBread());
        stringBuilder.append(" ");
        stringBuilder.append(sandwich.getCommodities());
        stringBuilder.append(" ");
        stringBuilder.append(sandwich.getMeat());
        System.out.println(stringBuilder.toString());

        System.out.println("=========================");
        //Prototype
        //Паттерн прототип применяется когда необходимо создать очень много объектов с "дорогим" созданием,
        // поэтому вместо этого объект просто копируется, но в результате создается уникальный экземпляр того же класса.
        // Но необходимо быть аккуратнее, так как поля - объекты сохраняют свои ссылки при
        // копировании, и можно изменить объект сразу у всех копий.
        Registry registry = new Registry();
        Movie interstellar = (Movie) registry.createItem("Movie");
        Movie anotherMovie = (Movie) registry.createItem("Movie");
        anotherMovie.setName("Keystone");

        Book book = (Book) registry.createItem("Book");
        System.out.println(book.getCover() + " " + book);
        System.out.println(interstellar.getOwner() + " " + interstellar.getName() + " " + interstellar);
        System.out.println(anotherMovie.getOwner() + " " + anotherMovie.getName() + " " + anotherMovie);

        System.out.println("=========================");
        //Factory
        //Паттерн фабрика определяет интерфейс для создания объектов, но не создает их, а оставляет
        //возможность классам-потомкам выбрать конкретную реализацию.
        Website blog = WebsiteFactory.createWebsite(WebsiteType.BLOG);
        Website shop = WebsiteFactory.createWebsite(WebsiteType.SHOP);
        System.out.println(blog.getList());
        System.out.println(shop.getList());
        //AbstractFactory
        //Паттерн абстрактная фабрика требуется когда нужно создать сложный объект, состоящий из других объектов
        //причем все составляющие объекты представляют одно семейство
        FoodFactory abstractFactory = FoodFactory.createFoodFactory(FoodType.FRUIT);
        Food food = abstractFactory.createResource(60);
        System.out.println(food + " " + food.getName());
    }
}
