package creational.AbstractFactory;

/**
 * Created by ink on 15.10.16.
 */
public class FruitFactory extends FoodFactory {
    @Override
    public Food createResource(int hunger) {
        if (hunger > 50)
            return new Mango();
        else
            return new Orange();
    }
}
