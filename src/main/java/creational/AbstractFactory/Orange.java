package creational.AbstractFactory;

/**
 * Created by ink on 15.10.16.
 */
public class Orange extends Food {
    public Orange() {
        this.name = "Orange";
    }
}
