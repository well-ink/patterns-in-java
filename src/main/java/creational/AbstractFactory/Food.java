package creational.AbstractFactory;

/**
 * Created by ink on 15.10.16.
 */
public abstract class Food {
    protected String name;

    public String getName() {
        return name;
    }
}
