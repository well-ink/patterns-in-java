package creational.AbstractFactory;

/**
 * Created by ink on 15.10.16.
 */
public class Mango extends Food {
    public Mango() {
        this.name = "Mango";
    }
}
