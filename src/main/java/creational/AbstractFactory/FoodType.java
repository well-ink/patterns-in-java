package creational.AbstractFactory;

/**
 * Created by ink on 15.10.16.
 */
public enum FoodType {
    FRUIT, VEGETABLE;
}
