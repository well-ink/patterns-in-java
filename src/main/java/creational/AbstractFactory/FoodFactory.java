package creational.AbstractFactory;

/**
 * Created by ink on 15.10.16.
 */
public abstract class FoodFactory {
   public static FoodFactory createFoodFactory(FoodType type) {
        switch (type) {
            case FRUIT:
                return new FruitFactory();
            case VEGETABLE:
                return new VegetableFactory();
            default:
                return null;
        }
    }

    public abstract Food createResource(int hunger);
}
