package creational.AbstractFactory;

/**
 * Created by ink on 15.10.16.
 */
public class VegetableFactory extends FoodFactory{
    @Override
    public Food createResource(int hunger) {
        if (hunger > 50)
            return new Tomato();
        else
            return new Cucumber();
    }
}
