package creational.AbstractFactory;

/**
 * Created by ink on 15.10.16.
 */
public class Tomato extends Food {
    public Tomato() {
        this.name = "Tomato";
    }
}
