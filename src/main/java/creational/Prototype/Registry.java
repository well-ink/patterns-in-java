package creational.Prototype;


import java.util.HashMap;
import java.util.Map;

public class Registry {
    private Map<String, Item> items = new HashMap<>();

    public Registry() {
        loadItems();
    }

    public Item createItem(String name) {
        Item item = null;
        try {
            item = (Item)items.get(name).clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return item;
    }

    private void loadItems() {
        Movie movie = new Movie();
        movie.setYear(2015);
        movie.setName("Interstellar");
        movie.setOwner(new Owner());

        Book book = new Book();
        book.setName("Leo");
        book.setCover("hard");

        items.put("Movie", movie);
        items.put("Book", book);
    }
}
