package creational.Prototype;

/**
 * Created by ink on 12.10.16.
 */
public class Movie extends Item {
    private int year;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
