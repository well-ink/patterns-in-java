package creational.Prototype;

/**
 * Created by ink on 12.10.16.
 */
public class Book extends Item {
    private String cover;

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
