package creational.Factory;

/**
 * Created by ink on 12.10.16.
 */
public class WebShop extends Website {
    @Override
    public void createWebsite() {
        list.add("Shop");
        list.add("Contacts");
        list.add("Delivery");
    }
}
