package creational.Factory;

/**
 * Created by ink on 12.10.16.
 */
public class WebsiteFactory {
    public static Website createWebsite(WebsiteType type) {
        switch (type) {
            case BLOG: {
                return new WebBlog();
            }
            case SHOP: {
                return new WebShop();
            }
            default: {
                return null;
            }
        }
    }
}
