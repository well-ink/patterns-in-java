package creational.Factory;

/**
 * Created by ink on 12.10.16.
 */
public enum WebsiteType {
    BLOG, SHOP;
}
