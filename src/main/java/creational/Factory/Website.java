package creational.Factory;

import java.util.ArrayList;
import java.util.List;


public abstract class Website {
    protected List<String> list = new ArrayList<>();

    public List<String> getList() {
        return list;
    }

    public Website() {
        this.createWebsite();
    }

    public abstract void createWebsite();
}
