package structural;

import structural.Adapter.LegacyUser;
import structural.Adapter.Tester;
import structural.Adapter.User;
import structural.Adapter.UserAdapter;
import structural.Composite.Leaf;
import structural.Composite.Tree;
import structural.Decorator.MeatDecorator;
import structural.Decorator.MustardDecorator;
import structural.Decorator.Sandwich;
import structural.Decorator.SimpleSandwich;
import structural.Facade.FacadeJdbcDemo;
import structural.Flyweight.InventorySystem;
import structural.Flyweight.Order;
import structural.Proxy.ServiceProxy;
import structural.Proxy.TwitterService;
import structural.Proxy.TwitterServiceStub;
import structural.Bridge.*;

import java.io.*;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class Main {

    public static void main(String[] args) {
        //structural.Adapter
        Tester.saySomething(new User(1, "Sergey", "Ivanov"));// Верное использование интерфейса - передаем реализацию интерфейса UserInterface
        Tester.saySomething(new UserAdapter(new LegacyUser(2, "Bad", "Boy")));// LegacyUser не реализует UserInterface, поэтому необходим класс - адаптер для использования метода

        System.out.println("\n--------------\n");
        //structural.Bridge
        Printer moviePrinter = new MoviePrinter(new Movie("Barbarian")); // Некоторый класс, который может экспортировать объект в зависимости от переданного в него "принтера"
        System.out.println(moviePrinter.print(new PrintFormatter())); // Можем создать любое количество принтеров, настраивая их по необходимости. Вывыодит данные о фильме в строку
        System.out.println(moviePrinter.print(new HtmlFormatter())); // Выводит те же самые данные о фильма в виде HTML

        System.out.println("\n--------------\n");
        //structural.Composite
        Tree mainTree = new Tree("structural.Main", "Nice");   // Паттерн Композит заключается в том, что объекты наследуются
        // от одного интерфейса и могут иметь сколь угодно глубокий уровень вложенности в друг друга
        // в итоге получается дерево компонентов, с каждым из которых можно общаться по одному и тому же интерфейсу
        Tree subTree = new Tree("Sub", "Not so nice"); // Здесь главное дерево содержит поддерево и поддерево содержит лист
        mainTree.add(subTree);
        Leaf leaf = new Leaf("Leaf", "Kek");
        subTree.add(leaf);
        System.out.println(mainTree.toString());// Если вызвать метод toString на главном корне дерева, будет совершен обход и вызван мето toString у всех потомков

        System.out.println("\n--------------\n");
        //structural.Decorator
        Sandwich sandwich = new MeatDecorator(new SimpleSandwich());// Декоратор должен только менять поведение объекта, поэтому декоратор
        // сам реализует интерфейс объекта
        Sandwich mustard = new MustardDecorator(new SimpleSandwich());  //Мы передаем с помощью композиции декорируемый объект,
        // и получаем экземпрял того же самого интерфейса что и переданный объект
        Sandwich dressedMeat = new MustardDecorator(new MeatDecorator(new SimpleSandwich())); // По тому же принципу работают и потоки в Java 8
        // Потоки изменяют поведение друг друга, изменяя переданные в них данные
        System.out.println(sandwich.make());
        System.out.println(mustard.make());
        System.out.println(dressedMeat.make());

        System.out.println("\n--------------\n");
        //structural.Facade
        FacadeJdbcDemo demo = new FacadeJdbcDemo(); //Паттерн Фасад облегчает работу со сложными интерфейсами, скрывая
        //подробности реализации и предоставляя удобные методы для работы
        demo.createDatabase(); //Примером из Java API является класс URL для работы с адресами в интернете
        demo.insertEntry();
        demo.insertEntry();
        for (FacadeJdbcDemo.Demo entry : demo.getEntries()) {
            System.out.println(entry.getId() + " - " + entry.getText());
        }
        demo.dropDatabase();

        System.out.println("\n--------------\n");
        //structural.Facade Java API
        try {
            URL url = new URL("http://eu.battle.net/ru/"); // За созданием объекта URL и его открытием скрывается множество деталей
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            File targetFile = new File("./data/file.txt");
            OutputStream outStream = new FileOutputStream(targetFile);

            while (reader.read() != -1) {
                outStream.write(reader.read());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\n--------------\n");
        //Nonblocking IO
        try (RandomAccessFile randomAccessFile = new RandomAccessFile("./data/nio-text.txt", "rw")) {
            FileChannel inChannel = randomAccessFile.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(48);
            int bytesRead = inChannel.read(buffer);
            while (bytesRead != -1) {
                System.out.println("Read " + bytesRead);
                buffer.flip();
                while (buffer.hasRemaining()) {
                    System.out.print((char) buffer.get());
                }
                buffer.clear();
                bytesRead = inChannel.read(buffer);
            }
        } catch (IOException e) {
            System.out.println("Problem with file " + e.getMessage());
        }

        System.out.println("\n--------------\n");
        //structural.Flyweight
        InventorySystem is = new InventorySystem();
        is.addOrder(new Order("Samsung", 12));
        is.addOrder(new Order("Samsung", 123));
        is.addOrder(new Order("Samsung", 124));//Создаем много экземпляров заказов с одним и тем же предметом.
        is.addOrder(new Order("LG", 2));
        is.addOrder(new Order("LG", 14));
        is.addOrder(new Order("LG", 16));

        is.printBucket(); // Проверяем что предмет в заказе - один и тот же, как и требовалось, так как предмет не изменяемый

        System.out.println("\n--------------\n");
        //structural.Proxy
        TwitterService service = (TwitterService) ServiceProxy.newInstance(new TwitterServiceStub());
        System.out.println(service.getTimeline("tryout"));
        try {
            service.postToTimeline("wow", "what");
        } catch (RuntimeException e) {
            System.out.println(e);
        }
    }
}
