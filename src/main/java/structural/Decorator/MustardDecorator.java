package structural.Decorator;

/**
 * Created by root on 25.09.16.
 */
public class MustardDecorator extends SandwichDecorator {
    public MustardDecorator(Sandwich sandwich) {
        super(sandwich);
    }

    @Override
    public String make() {
        return decoratedSandwich.make() + " and mustard";
    }
}
