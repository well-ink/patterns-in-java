package structural.Decorator;

/**
 * Created by root on 25.09.16.
 */
public abstract class SandwichDecorator implements Sandwich {
    protected Sandwich decoratedSandwich;

    public SandwichDecorator(Sandwich decoratedSandwich) {
        this.decoratedSandwich = decoratedSandwich;
    }
}
