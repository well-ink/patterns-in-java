package structural.Decorator;

/**
 * Created by root on 25.09.16.
 */
public interface Sandwich {
    String make();
}
