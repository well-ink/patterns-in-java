package structural.Decorator;

/**
 * Created by root on 25.09.16.
 */
public class SimpleSandwich implements Sandwich {
    @Override
    public String make() {
        return "Bread";
    }
}
