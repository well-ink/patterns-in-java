package structural.Decorator;

/**
 * Created by root on 25.09.16.
 */
public class MeatDecorator extends SandwichDecorator {
    public MeatDecorator(Sandwich sandwich) {
        super(sandwich);
    }

    @Override
    public String make() {
        return decoratedSandwich.make() + " with meat";
    }
}
