package structural.Bridge;

import java.util.List;

/**
 * Created by ink on 23.09.16.
 */
public interface Formatter {
    String format(String header, List<Detail> details);
}
