package structural.Bridge;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ink on 23.09.16.
 */
public class MoviePrinter extends Printer {
    private Movie movie;

    public MoviePrinter(Movie movie) {
        this.movie = movie;
    }

    @Override
    public String getHeader() {
        return movie.getHeader();
    }

    @Override
    public List<Detail> getDetails() {
        List<Detail> details = new ArrayList<>();
        details.add(new Detail("Actor", "Leo DiCaprio"));
        details.add(new Detail("Year", "1994"));
        details.add(new Detail("Mark", "90/100"));
        return details;
    }
}
