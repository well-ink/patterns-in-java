package structural.Bridge;

import java.util.List;

/**
 * Created by ink on 23.09.16.
 */
public class HtmlFormatter implements Formatter {
    @Override
    public String format(String header, List<Detail> details) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("<h1>");
        stringBuilder.append(header);
        stringBuilder.append("<h1>");

        for (Detail detail : details) {
            stringBuilder.append("<p>");
            stringBuilder.append(detail.getKey() + " - " + detail.getValue());
            stringBuilder.append("<p>");
        }

        return stringBuilder.toString();
    }
}