package structural.Bridge;

/**
 * Created by ink on 23.09.16.
 */
class Detail {
    private String key;
    private String value;

    Detail(String key, String value) {
        this.key = key;
        this.value = value;
    }

    String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
