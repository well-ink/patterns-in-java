package structural.Bridge;

/**
 * Created by ink on 23.09.16.
 */
public class Movie {
    private String header;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Movie(String header) {

        this.header = header;
    }
}
