package structural.Bridge;

import java.util.List;

/**
 * Created by ink on 23.09.16.
 */
public class PrintFormatter implements Formatter {
    @Override
    public String format(String header, List<Detail> details) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(header);
        stringBuilder.append("\n");
        for (Detail detail : details) {
            stringBuilder.append(detail.getKey());
            stringBuilder.append(" - ");
            stringBuilder.append(detail.getValue());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
}
