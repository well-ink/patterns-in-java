package structural.Bridge;

import java.util.List;

/**
 * Created by ink on 23.09.16.
 */
public abstract class Printer {

    public String print(Formatter formatter) {
        return formatter.format(getHeader(), getDetails());
    }

    public abstract String getHeader();

    public abstract List<Detail> getDetails();
}
