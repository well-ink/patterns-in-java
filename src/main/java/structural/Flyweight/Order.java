package structural.Flyweight;

import java.util.HashMap;

/**
 * Created by ink on 09.10.16.
 */
public class Order {
    private Item item;
    private int number;
    private final static HashMap<String, Item> itemDict = new HashMap<>(); //Содержит экземпляры неизменяемых классов (данные)

    public Order(String itemName, int orderNumber) {
        if (!itemDict.containsKey(itemName)) {//Вместо создания множества новых экземпляров,
            item = new Item(itemName);// мы берем уже существующие из карты, это может существенно сэкономить память
            itemDict.put(itemName, item);

        }

        item = itemDict.get(itemName);
        number = orderNumber;
    }

    @Override
    public String toString() {
        return System.identityHashCode(item) + " " + item.getName() + " " + number;
    }
}
