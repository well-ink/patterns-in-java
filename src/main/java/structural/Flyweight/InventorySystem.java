package structural.Flyweight;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ink on 09.10.16.
 */
public class InventorySystem {
    private List<Order> bucket = null;

    public InventorySystem() {
        this.bucket = new ArrayList<>();
    }

    public void addOrder(Order order) {
        bucket.add(order);
    }

    public void printBucket() {
        for (Order order : bucket) {
            System.out.println(order);
        }
    }
}
