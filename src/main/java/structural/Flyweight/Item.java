package structural.Flyweight;

/**
 * Created by ink on 09.10.16.
 */
public class Item {
    private String name;

    public Item(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
