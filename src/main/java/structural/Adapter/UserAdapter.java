package structural.Adapter;

/**
 * Получает в качестве параметра легаси-пользователя, которого необходимо передать в метод, где ожидается UserInterface, преобразует его к необходимому виду.
 */
public class UserAdapter implements UserInterface {
    private LegacyUser instance;

    public UserAdapter(LegacyUser instance) {
        this.instance = instance;
    }


    @Override
    public int getId() {
        return instance.getLegacyId();
    }

    @Override
    public void setId(int id) {
        instance.setLegacyId(id);
    }

    @Override
    public String getFirstName() {
        return instance.getName();
    }

    @Override
    public void setFirstName(String firstName) {
        instance.setName(firstName);
    }

    @Override
    public String getLastName() {
        return instance.getSurname();
    }

    @Override
    public void setLastName(String lastName) {
        instance.setSurname(lastName);
    }
}
