package structural.Adapter;

/**
 * Демонстрация паттерна structural.Adapter
 */
public class Tester {
    public static void saySomething(UserInterface user) {
        System.out.println(user + " says " + "Kek");
    }
}
