package structural.Adapter;

/**
 * Базовый интерфейс пользователя
 */
public interface UserInterface {
    int getId();

    void setId(int id);

    public String getFirstName();

    public void setFirstName(String firstName);

    public String getLastName();

    public void setLastName(String lastName);
}
