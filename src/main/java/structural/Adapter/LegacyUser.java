package structural.Adapter;

/**
 * Старый формат пользователя, не подходит для использования с новым API, поэтому ему необходим класс адаптер.
 */
public class LegacyUser {
    private int legacyId;
    private String name;
    private String surname;

    public int getLegacyId() {
        return legacyId;
    }

    public void setLegacyId(int legacyId) {
        this.legacyId = legacyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LegacyUser(int legacyId, String name, String surname) {

        this.legacyId = legacyId;
        this.name = name;
        this.surname = surname;
    }
}
