package structural.Composite;

import java.util.Iterator;

/**
 * Created by root on 25.09.16.
 */
public class Tree extends Component {
    public Tree(String name, String text) {
        this.name = name;
        this.text = text;
    }

    @Override
    public Component add(Component component) {
        children.add(component);
        return component;
    }

    @Override
    public Component remove(Component component) {
        children.remove(component);
        return component;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(print(this));

        Iterator<Component> iterator = children.iterator();
        while (iterator.hasNext()) {
            builder.append(iterator.next().toString());
        }
        return builder.toString();
    }
}
