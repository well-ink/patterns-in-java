package structural.Composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 25.09.16.
 */
public abstract class Component {
    protected String name;
    protected String text;
    protected List<Component> children = new ArrayList<>();

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public Component add(Component component) {
        throw new UnsupportedOperationException("Not implemented at this level");
    }

    public Component remove(Component component) {
        throw new UnsupportedOperationException("Not implemented at this level");
    }

    String print(Component component) {
        StringBuilder builder = new StringBuilder(name);
        builder.append(": ");
        builder.append(text);
        builder.append("\n");
        return builder.toString();
    }

    public abstract String toString();
}
