package structural.Composite;

/**
 * Created by root on 25.09.16.
 */
public class Leaf extends Component {

    public Leaf(String name, String text) {
        this.name = name;
        this.text = text;
    }

    @Override
    public String toString() {
        return name + ": " + text + "\n";
    }
}
