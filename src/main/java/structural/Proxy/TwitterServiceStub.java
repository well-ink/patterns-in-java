package structural.Proxy;

/**
 * Created by ink on 09.10.16.
 */
public class TwitterServiceStub implements TwitterService {
    @Override
    public String getTimeline(String name) {
        return "Stub timeline";
    }

    @Override
    public void postToTimeline(String name, String message) {
        System.out.println("This message shouldn't go through");
    }
}
