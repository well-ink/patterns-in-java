package structural.Proxy;

/**
 * Created by ink on 09.10.16.
 */
public interface TwitterService {
    String getTimeline(String name);
    void postToTimeline(String name, String message);
}
