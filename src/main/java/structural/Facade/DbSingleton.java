package structural.Facade;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by ink on 27.09.16.
 */
public class DbSingleton {
    private static final String JDBC_DRIVER = "org.h2.Driver";
    private static final String ADDRESS = "jdbc:h2:./data/test";

    private static DbSingleton instance;

    private DbSingleton() {
    }

    public static DbSingleton getInstance() {
        if (instance == null) {
            instance = new DbSingleton();
        }
        return instance;
    }

    public Connection getConnection() {

        Connection connection = null;
        try {
            connection = DriverManager.getConnection(ADDRESS);
        } catch (SQLException e) {
            System.out.println("Problem with connection to DB");
            e.printStackTrace();
        }
        return connection;
    }
}
