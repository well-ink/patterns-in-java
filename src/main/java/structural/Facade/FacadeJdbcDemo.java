package structural.Facade;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ink on 27.09.16.
 */
public class FacadeJdbcDemo {
    private DbSingleton dbSingleton;

    public FacadeJdbcDemo() {
        dbSingleton = DbSingleton.getInstance();
    }

    public void createDatabase() {
        try (Connection connection = dbSingleton.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("CREATE TABLE IF NOT EXISTS Demo(ID INT PRIMARY KEY AUTO_INCREMENT, TEXT VARCHAR(255) DEFAULT '')");
        } catch (SQLException e) {
            System.out.println("Error creating table");
        }
    }

    public void dropDatabase() {
        try (Connection connection = dbSingleton.getConnection();
             Statement statement = connection.createStatement()) {
            statement.execute("DROP TABLE Demo");
        } catch (SQLException e) {
            System.out.println("Error dropping table");
        }
    }

    public int insertEntry() {
        int result = 0;
        try (Connection connection = dbSingleton.getConnection();
             Statement statement = connection.createStatement()) {
            result = statement.executeUpdate("INSERT INTO Demo(TEXT) VALUES ('Lol kek, here it comes!')");
        } catch (SQLException e) {
            System.out.println("Error inserting entry");
        }
        return result;
    }

    public List<Demo> getEntries() {
        List<Demo> result = new ArrayList<>();
        try (Connection connection = dbSingleton.getConnection();
             Statement statement = connection.createStatement()) {
            ResultSet set = statement.executeQuery("SELECT * FROM Demo");
            while (set.next()) {
                Demo demo = new Demo();
                demo.setId(set.getInt(1));
                demo.setText(set.getString(2));
                result.add(demo);
            }
        } catch (SQLException e) {
            System.out.println("Error inserting entry");
        }
        return result;
    }

    public class Demo {
        private String text;
        private int id;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }
}
