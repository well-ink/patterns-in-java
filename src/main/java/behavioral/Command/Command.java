package behavioral.Command;

/**
 * Created by ink on 16.10.16.
 */
public interface Command {
    void execute();
}
