package behavioral.Command;

/**
 * Created by ink on 16.10.16.
 */
public class OnCommand implements Command {
    private Light light;

    public OnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        System.out.println("Turning the " + light + " on");
        light.turnOn();
    }
}
