package behavioral.Command;

/**
 * Created by ink on 16.10.16.
 */
public class Light {
    private boolean state = false;

    public void turnOn() {
        state = true;
    }
}
