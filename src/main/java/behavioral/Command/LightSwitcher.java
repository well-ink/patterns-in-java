package behavioral.Command;

/**
 * Created by ink on 16.10.16.
 */
public class LightSwitcher {
    public static void storeAndExecute(Command command) {
        command.execute();
    }
}
