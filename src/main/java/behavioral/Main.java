package behavioral;

import behavioral.ChainOfResponsibility.Accountant;
import behavioral.ChainOfResponsibility.Director;
import behavioral.ChainOfResponsibility.Owner;
import behavioral.ChainOfResponsibility.Request;
import behavioral.Command.Command;
import behavioral.Command.Light;
import behavioral.Command.LightSwitcher;
import behavioral.Command.OnCommand;


/**
 * Created by ink on 16.10.16.
 */
public class Main {
    public static void main(String[] args) {
        //Chain of Responsibility pattern
        //Паттерн необходим для отделения запроса от исполнителя и клиента, отправляющего запрос.
        //Клиент не знает, на каком участке цепи будет выполнен его запрос
        Accountant accountant = new Accountant();
        Director director = new Director();
        Owner owner = new Owner();

        accountant.setSuccessor(director);
        director.setSuccessor(owner);

        Request bill = new Request(Request.RequestType.BILL, 999);
        Request hugeBill = new Request(Request.RequestType.BILL, 10000);
        Request conference = new Request(Request.RequestType.CONFERENCE, 500);

        accountant.handleRequest(bill);
        accountant.handleRequest(hugeBill);
        accountant.handleRequest(conference);

        System.out.println("====================");
        //Command pattern
        //Паттерн команда применяется, когда нужно инкапсулировать команду (действие) в виде объекта
        //так же предоставляет возможность отмены совершенного действия
        Light bedroomLight = new Light();
        Light kitchenLight = new Light();
        Command bedroomOnCommand = new OnCommand(bedroomLight);
        Command kitchenOnCommand = new OnCommand(kitchenLight);
        LightSwitcher.storeAndExecute(bedroomOnCommand);
        LightSwitcher.storeAndExecute(kitchenOnCommand);
    }
}
