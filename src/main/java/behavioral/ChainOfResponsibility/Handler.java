package behavioral.ChainOfResponsibility;

/**
 * Created by ink on 16.10.16.
 */
public abstract class Handler {
    protected Handler successor;

    public void setSuccessor(Handler successor) {
        this.successor = successor;
    }

    public abstract void handleRequest(Request request);
}
