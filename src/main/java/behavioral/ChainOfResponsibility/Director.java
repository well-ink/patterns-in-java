package behavioral.ChainOfResponsibility;

/**
 * Created by ink on 16.10.16.
 */
public class Director extends Handler {
    @Override
    public void handleRequest(Request request) {
        if (request.getRequestType() == Request.RequestType.BILL)
            System.out.println("Director can pay any bills");
        else
            successor.handleRequest(request);
    }
}
