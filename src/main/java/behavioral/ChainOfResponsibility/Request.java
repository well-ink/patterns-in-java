package behavioral.ChainOfResponsibility;

/**
 * Created by ink on 16.10.16.
 */
public class Request {
    private RequestType requestType;
    private int sum;

    public Request(RequestType requestType, int sum) {
        this.requestType = requestType;
        this.sum = sum;
    }

    public RequestType getRequestType() {
        return requestType;
    }

    public int getSum() {
        return sum;
    }

    public enum RequestType {
        BILL, CONFERENCE
    }
}
