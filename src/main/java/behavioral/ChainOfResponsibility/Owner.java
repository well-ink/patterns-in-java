package behavioral.ChainOfResponsibility;

/**
 * Created by ink on 16.10.16.
 */
public class Owner extends Handler {
    @Override
    public void handleRequest(Request request) {
        System.out.println("Owner can do anything he wants, even conferences");
    }
}
