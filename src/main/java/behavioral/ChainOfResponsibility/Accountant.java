package behavioral.ChainOfResponsibility;

/**
 * Created by ink on 16.10.16.
 */
public class Accountant extends Handler {
    @Override
    public void handleRequest(Request request) {
        if (request.getRequestType() == Request.RequestType.BILL && request.getSum() < 1000)
            System.out.println("Accountant can pay bills less than 1000");
        else
            successor.handleRequest(request);
    }
}
